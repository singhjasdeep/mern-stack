/* -----------------------------------------------------------------------
   * @ description : This is the util controller layer.
----------------------------------------------------------------------- */
import fs from 'fs';
import { uploader, imageFilter } from '../utilities/universal';
import { failAction, successAction } from '../utilities/rest';
import { imageFileExtensions } from '../utilities/constants';

// import Messages from '../utilities/messages';
// import logger from '../utilities/logger';

const UPLOAD_PATH = 'assets/';
const fileOptions = { dest: UPLOAD_PATH, fileFilter: imageFilter };

export const downloadFile = {
  directory: {
    path: UPLOAD_PATH,
    redirectToSlash: false,
    index: false
  }
};

export const uploadFile = async (request, h) => {
  const { payload } = request;
  const file = payload['file'];
  const fileExtension = file.hapi.filename.split('.').pop();
  if (imageFileExtensions.includes(fileExtension.toLowerCase())) {
    fileOptions.dest = `${fileOptions.dest}images/`;
  } else {
    fileOptions.dest = `${fileOptions.dest}media/`;
  }
  if (!fs.existsSync(fileOptions.dest)) {
    fs.mkdirSync(fileOptions.dest);
  }
  try {
    const data = await uploader(file, fileOptions);
    data.filePath = fileOptions.dest;
    return successAction(data);
  } catch (error) {
    failAction(error.message);
  }
};
