import Mongoose from 'mongoose';
import { getTimeStamp } from '../../utilities/universal';

const Schema = Mongoose.Schema;

const UserSchema = new Schema({
  fullName: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  role: { type: String, required: true, default: 'user' }, // business, user, admin
  verified: {
    token: { type: String, default: '' },
    status: { type: Boolean, default: false }
  },
  loginToken: [
    {
      token: { type: String, default: '' },
      when: { type: Number, default: getTimeStamp }
    }
  ],
  lastLogin: { type: Number },
  isActive: { type: Boolean, default: true },
  location: {
    type: {
      type: String,
      enum: ['Point'],
      default: 'Point',
    },
    coordinates: {
      type: [Number],
      default: [0, 0],
    }
  },
  createdAt: { type: Number, default: getTimeStamp },
  updatedAt: { type: Number, default: getTimeStamp }
});
export default UserSchema.index({ location: '2dsphere' });