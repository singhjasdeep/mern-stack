import v1 from './v1';
import file from './file';

export default [...v1, ...file];
