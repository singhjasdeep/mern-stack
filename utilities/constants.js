/* -----------------------------------------------------------------------
   * @ description : Main module to include all the constants used in project.
----------------------------------------------------------------------- */

export const imageFileExtensions = ['jpeg', 'gif', 'bmp', 'jpg', 'png'];