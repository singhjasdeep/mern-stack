/* -----------------------------------------------------------------------
   * @ description : This file defines the user schema for mongodb.
----------------------------------------------------------------------- */

import Mongoose from 'mongoose';
import DbSchema from './DbSchema';
import { getTimeStamp } from '../../utilities/universal';

class UserClass {
  static checkEmail(email) {
    return this.findOne({ email });
  }
  static checkToken(token) {
    return this.findOne({ 'loginToken.token': token });
  }
  static register(payload) {
    return this(payload).save();
  }
  static login(email, password) {
    return this.findOne({
      email,
      password
    });
  }
  static onLoginDone(userId, payload, loginToken) {
    let updateData = {
      $push: { loginToken: { token: loginToken } },
      $set: {
        lastLogin: getTimeStamp(),
        updatedAt: getTimeStamp()
      }
    };

    return this.findByIdAndUpdate(userId, updateData, { new: true });
  }
  static logout(userId, token) {
    let updateData = {
      $set: {
        'device.token': '',
        updatedAt: getTimeStamp()
      },
      $pull: { loginToken: { token } }
    };
    return this.findByIdAndUpdate(userId, updateData);
  }
}

DbSchema.loadClass(UserClass);

export default Mongoose.model('User', DbSchema);
